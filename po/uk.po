# Ukrainian translation for guvcview
# Copyright (c) 2010 Rosetta Contributors and Canonical Ltd 2010
# This file is distributed under the same license as the guvcview package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: guvcview\n"
"Report-Msgid-Bugs-To: pj.assis@gmail.com\n"
"POT-Creation-Date: 2013-11-14 22:15+0000\n"
"PO-Revision-Date: 2013-02-25 05:37+0000\n"
"Last-Translator: Paulo Assis <pj.assis@gmail.com>\n"
"Language-Team: Ukrainian <uk@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2013-12-08 10:36+0000\n"
"X-Generator: Launchpad (build 16869)\n"
"Language: uk\n"

#: ../data/guvcview.desktop.in.in.h:1
msgid "guvcview"
msgstr ""

#: ../data/guvcview.desktop.in.in.h:2
msgid "GTK UVC video viewer"
msgstr ""

#: ../data/guvcview.desktop.in.in.h:3
msgid "A video viewer and capturer for the linux uvc driver"
msgstr ""

#: ../src/acodecs.c:59
msgid "PCM - uncompressed (16 bit)"
msgstr ""

#: ../src/acodecs.c:76
msgid "MPEG2 - (lavc)"
msgstr ""

#: ../src/acodecs.c:93
msgid "MP3 - (lavc)"
msgstr ""

#: ../src/acodecs.c:110
msgid "Dolby AC3 - (lavc)"
msgstr ""

#: ../src/acodecs.c:127
msgid "ACC Low - (faac)"
msgstr ""

#: ../src/acodecs.c:144
msgid "Vorbis"
msgstr ""

#: ../src/audio_tab.c:87
msgid "Audio"
msgstr "Аудіо"

#: ../src/audio_tab.c:105
msgid " Sound"
msgstr " Звук"

#. VU meter on the image (OSD)
#: ../src/audio_tab.c:116
msgid " Show VU meter"
msgstr ""

#: ../src/audio_tab.c:128
msgid "Audio API:"
msgstr "Аудіо API:"

#: ../src/audio_tab.c:139
msgid "PORTAUDIO"
msgstr "PORTAUDIO"

#: ../src/audio_tab.c:140
msgid "PULSEAUDIO"
msgstr "PULSEAUDIO"

#: ../src/audio_tab.c:156 ../src/audio_tab.c:179
msgid "Input Device:"
msgstr "Пристрій вводу:"

#: ../src/audio_tab.c:189 ../src/audio_tab.c:221
msgid "Dev. Default"
msgstr "Пристрій по замовч."

#: ../src/audio_tab.c:211
msgid "Sample Rate:"
msgstr "Частота дискредитації:"

#: ../src/audio_tab.c:222
msgid "1 - mono"
msgstr "1 - моно"

#: ../src/audio_tab.c:223
msgid "2 - stereo"
msgstr "2 - стерео"

#: ../src/audio_tab.c:258
msgid "Channels:"
msgstr "Канали:"

#: ../src/audio_tab.c:309
msgid "---- Audio Effects ----"
msgstr "---- Ефекти аудіо ----"

#. Echo
#: ../src/audio_tab.c:328
msgid " Echo"
msgstr " Ехо"

#. FUZZ
#: ../src/audio_tab.c:339
msgid " Fuzz"
msgstr " Пух"

#. Reverb
#: ../src/audio_tab.c:351
msgid " Reverb"
msgstr " Ревербація"

#. WahWah
#: ../src/audio_tab.c:363
msgid " WahWah"
msgstr " ВахВах"

#. Ducky
#: ../src/audio_tab.c:375
msgid " Ducky"
msgstr " Голубка"

#: ../src/callbacks.c:109
msgid "Error"
msgstr "Помилка"

#: ../src/callbacks.c:133
msgid ""
"\n"
"You have more than one video device installed.\n"
"Do you want to try another one ?\n"
msgstr ""
"\n"
"Встановлено більше одного відео пристрою.\n"
"Ви хочете спробувати ще один?\n"

#. Devices
#: ../src/callbacks.c:140 ../src/video_tab.c:111
msgid "Device:"
msgstr "Пристрій:"

#: ../src/callbacks.c:272
msgid "Save File"
msgstr "Зберегти"

#: ../src/callbacks.c:284
msgid "File Format:"
msgstr ""

#: ../src/callbacks.c:455
msgid "codec values"
msgstr "значення кодека"

#: ../src/callbacks.c:466
msgid ""
"                              encoder fps:   \n"
" (0 - use fps combobox value)"
msgstr ""
"                              частота кадрів:   \n"
" (0 - використати значення з випадаючого списку)"

#: ../src/callbacks.c:479
msgid " monotonic pts"
msgstr ""

#. bit rate
#: ../src/callbacks.c:486 ../src/callbacks.c:809
msgid "bit rate:   "
msgstr "бітрейт:   "

#: ../src/callbacks.c:499
msgid "qmax:   "
msgstr "qmax:   "

#: ../src/callbacks.c:512
msgid "qmin:   "
msgstr "qmin:   "

#: ../src/callbacks.c:525
msgid "max. qdiff:   "
msgstr "макс. qdiff:   "

#: ../src/callbacks.c:538
msgid "dia size:   "
msgstr "dia size:   "

#: ../src/callbacks.c:551
msgid "pre dia size:   "
msgstr "pre dia size:   "

#: ../src/callbacks.c:564
msgid "pre me:   "
msgstr "pre me:   "

#: ../src/callbacks.c:577
msgid "pre cmp:   "
msgstr "pre cmp:   "

#: ../src/callbacks.c:590
msgid "cmp:   "
msgstr "cmp:   "

#: ../src/callbacks.c:603
msgid "sub cmp:   "
msgstr "sub cmp:   "

#: ../src/callbacks.c:616
msgid "last predictor count:   "
msgstr "значення лічильника прогнозиста:   "

#: ../src/callbacks.c:629
msgid "gop size:   "
msgstr "gop size:   "

#: ../src/callbacks.c:642
msgid "qcompress:   "
msgstr "qcompress   "

#: ../src/callbacks.c:655
msgid "qblur:   "
msgstr "qblur:   "

#: ../src/callbacks.c:668
msgid "subq:   "
msgstr "subq:   "

#: ../src/callbacks.c:681
msgid "framerefs:   "
msgstr "framerefs:   "

#: ../src/callbacks.c:694
msgid "me method:   "
msgstr "me method:   "

#: ../src/callbacks.c:707
msgid "mb decision:   "
msgstr "mb decision:   "

#: ../src/callbacks.c:720
msgid "max B frames:   "
msgstr "max B frames:   "

#: ../src/callbacks.c:733
msgid "num threads:   "
msgstr ""

#: ../src/callbacks.c:796
msgid "audio codec values"
msgstr "значення аудіо-кодека"

#. sample format
#: ../src/callbacks.c:823
msgid "sample format:   "
msgstr ""

#: ../src/callbacks.c:1311
msgid "start new"
msgstr "запустити новий"

#: ../src/callbacks.c:1314
msgid "restart"
msgstr "перезавантаж."

#: ../src/callbacks.c:1316
msgid "new"
msgstr "новий"

#: ../src/callbacks.c:1318
msgid "cancel"
msgstr "відмінити"

#: ../src/callbacks.c:1323
msgid ""
"launch new process or restart?.\n"
"\n"
msgstr ""
"запустити новий процес, \n"
"чи перезавантажити?\n"
"\n"

#: ../src/callbacks.c:1780 ../src/guvcview.c:565
msgid "Cap. Image (I)"
msgstr ""

#. disable image controls
#: ../src/callbacks.c:1791 ../src/guvcview.c:690 ../src/timers.c:101
msgid "capturing photo to"
msgstr ""

#: ../src/callbacks.c:1850 ../src/guvcview.c:575
msgid "Cap. Video (V)"
msgstr ""

#: ../src/callbacks.c:1876 ../src/guvcview.c:740
msgid "capturing video to"
msgstr ""

#. vid capture enabled from start
#: ../src/callbacks.c:1911 ../src/guvcview.c:570
msgid "Stop Video (V)"
msgstr ""

#: ../src/callbacks.c:1967 ../src/menubar.c:66
msgid "Save Profile"
msgstr ""

#: ../src/callbacks.c:1980 ../src/menubar.c:65
msgid "Load Profile"
msgstr ""

#: ../src/create_video.c:1098 ../src/guvcview.c:732
msgid "Cap. Video"
msgstr "Відео"

#: ../src/guvcview.c:280
msgid "Guvcview Video Capture"
msgstr "Захоплювач відео Guvcview"

#: ../src/guvcview.c:292
msgid "GUVCViewer Controls"
msgstr "Панель налаштувань Guvcview"

#. can't open device
#: ../src/guvcview.c:342
msgid ""
"Guvcview error:\n"
"\n"
"Unable to open device"
msgstr ""
"Помилка:\n"
"\n"
"Не можу відкрити пристрій"

#: ../src/guvcview.c:343
msgid ""
"Please make sure the camera is connected\n"
"and that the correct driver is installed."
msgstr ""
"Переконайтесь, що камера підключена\n"
"і встановлено відповідний драйвер."

#. uvc extension controls OK, give warning and shutdown (called with --add_ctrls)
#: ../src/guvcview.c:348
msgid ""
"Guvcview:\n"
"\n"
"UVC Extension controls"
msgstr ""
"Guvcview:\n"
"\n"
"Елементи керування розширенням UVC"

#: ../src/guvcview.c:349
msgid "Extension controls were added to the UVC driver"
msgstr "Керуючі елементи розширення були додані до драйверу UVC"

#. uvc extension controls error - EACCES (needs root user)
#: ../src/guvcview.c:356
msgid ""
"Guvcview error:\n"
"\n"
"UVC Extension controls"
msgstr ""
"Помилка:\n"
"\n"
"Керуючі елементи UVC"

#: ../src/guvcview.c:357
msgid ""
"An error occurred while adding extension\n"
"controls to the UVC driver\n"
"Make sure you run guvcview as root (or sudo)."
msgstr ""
"Виникла помилка в процесі додавання керуючих \n"
"елементів розширення до драйверу UVC.\n"
"Переконайтесь що Guvcview запущено від імені root (чи sudo)."

#: ../src/guvcview.c:391 ../src/guvcview.c:399 ../src/guvcview.c:477
msgid ""
"Guvcview error:\n"
"\n"
"Can't set a valid video stream for guvcview"
msgstr ""
"Помилка:\n"
"\n"
"Не можу встановити правильний відео потік для Guvcview"

#: ../src/guvcview.c:392 ../src/guvcview.c:400 ../src/guvcview.c:478
msgid ""
"Make sure your device driver is v4l2 compliant\n"
"and that it is properly installed."
msgstr ""
"Переконайтесь, що драйвер сумісний з v4l2\n"
"і встановлений правильно."

#: ../src/guvcview.c:422 ../src/guvcview.c:444
msgid ""
"Guvcview error:\n"
"\n"
"Unable to start with minimum setup"
msgstr ""
"Помилка:\n"
"\n"
"Не можу запуститись з мінімальною установкою"

#: ../src/guvcview.c:423 ../src/guvcview.c:445
msgid "Please reconnect your camera."
msgstr "Будь ласка, повторно підключіть свою камеру."

#: ../src/guvcview.c:452
msgid ""
"Guvcview error:\n"
"\n"
"Couldn't query device capabilities"
msgstr ""
"Помилка:\n"
"\n"
"Не можу надіслати запит про можливості пристрою"

#: ../src/guvcview.c:453
msgid "Make sure the device driver supports v4l2."
msgstr "Переконайтесь, що драйвер пристрою підтримує v4l2"

#: ../src/guvcview.c:457
msgid ""
"Guvcview error:\n"
"\n"
"Read method error"
msgstr ""
"Помилка:\n"
"\n"
"Помилка методу зчитування."

#: ../src/guvcview.c:458
msgid "Please try mmap instead (--capture_method=1)."
msgstr "Будь ласка, спробуйте mmap instead (--capture_method=1)."

#: ../src/guvcview.c:466
msgid ""
"Guvcview error:\n"
"\n"
"Unable to allocate Buffers"
msgstr ""
"Помилка:\n"
"\n"
"Не можу виділити місце під Буфери."

#: ../src/guvcview.c:467
msgid "Please try restarting your system."
msgstr "Будь ласка, спробуйте перезавантажити систему."

#: ../src/guvcview.c:518
msgid "Image Controls"
msgstr ""
"Керування\n"
"зображенням"

#. image auto capture
#: ../src/guvcview.c:561
msgid "Stop Auto (I)"
msgstr ""

#: ../src/guvcview.c:676
msgid ""
"Guvcview error:\n"
"\n"
"Unable to create Video Thread"
msgstr ""
"Помилка:\n"
"\n"
"Не можу створити потік команд для відео"

#: ../src/guvcview.c:677
msgid "Please report it to http://developer.berlios.de/bugs/?group_id=8179"
msgstr ""
"Будь ласка, повідомте про це тут "
"http://developer.berlios.de/bugs/?group_id=8179"

#: ../src/img_controls.c:121
msgid "Auto Focus (continuous)"
msgstr "Авто фокус (безперервний)"

#: ../src/img_controls.c:122
msgid "set Focus"
msgstr "встановити фокус"

#: ../src/image_format.c:35
msgid "Jpeg (jpg)"
msgstr ""

#: ../src/image_format.c:41
msgid "Bitmap (Bmp)"
msgstr ""

#: ../src/image_format.c:47
msgid "Portable Network Graphics (Png)"
msgstr ""

#: ../src/image_format.c:53
msgid "Raw Image (raw)"
msgstr ""

#. controls menu
#: ../src/menubar.c:64
msgid "Settings"
msgstr ""

#: ../src/menubar.c:67
msgid "Hardware Defaults"
msgstr ""

#: ../src/menubar.c:75
msgid "Camera Button"
msgstr ""

#: ../src/menubar.c:79
msgid "Capture Image"
msgstr ""

#: ../src/menubar.c:84
msgid "Capture Video"
msgstr ""

#. video menu
#: ../src/menubar.c:144 ../src/video_tab.c:95
msgid "Video"
msgstr ""

#: ../src/menubar.c:145 ../src/menubar.c:233
msgid "File"
msgstr ""

#: ../src/menubar.c:146 ../src/menubar.c:234
msgid "Increment Filename"
msgstr ""

#: ../src/menubar.c:164
msgid "Video Codec"
msgstr ""

#: ../src/menubar.c:189
msgid "Video Codec Properties"
msgstr ""

#: ../src/menubar.c:196
msgid "Audio Codec"
msgstr ""

#: ../src/menubar.c:217
msgid "Audio Codec Properties"
msgstr ""

#. photo menu
#: ../src/menubar.c:232
msgid "Photo"
msgstr ""

#: ../src/options.c:753
msgid "Prints version"
msgstr "Показує версію"

#: ../src/options.c:754
msgid "Displays debug information"
msgstr "Показує інформацію для відлагоджування"

#: ../src/options.c:755
msgid "Video Device to use [default: /dev/video0]"
msgstr "Використаний відео-пристрій [default: /dev/video0]"

#: ../src/options.c:756
msgid "Exit after adding UVC extension controls (needs root/sudo)"
msgstr ""
"Вийти після додавання керуючих елементів розширення UVC\n"
"(потрібні права адміністратора)"

#: ../src/options.c:757
msgid "Don't stream video (image controls only)"
msgstr "Не потокове відео (керувати лише зображенням)"

#: ../src/options.c:758
msgid "Don't display a GUI"
msgstr ""

#: ../src/options.c:759
msgid "Capture method (1-mmap (default)  2-read)"
msgstr "Метод захоплення (1-mmap (замовч.) 2-read)"

#: ../src/options.c:760
msgid "Configuration file"
msgstr "Файл конфігурації"

#: ../src/options.c:761
msgid "Hardware accelaration (enable(1) | disable(0))"
msgstr "Апаратне прискорення (увімкн.(1) | вимкн.(0))"

#: ../src/options.c:762
msgid ""
"Pixel "
"format(mjpg|jpeg|yuyv|yvyu|uyvy|yyuv|yu12|yv12|nv12|nv21|nv16|nv61|y41p|grey|"
"y10b|y16 |s501|s505|s508|gbrg|grbg|ba81|rggb|bgr3|rgb3)"
msgstr ""

#: ../src/options.c:763
msgid "Frame size, default: 640x480"
msgstr "Розмір кадру (замовч. - 640x480)"

#: ../src/options.c:764
msgid "Image File name"
msgstr "Файл зображення"

#: ../src/options.c:765
msgid "Image capture interval in seconds"
msgstr "Інтервал між знімками (сек.)"

#: ../src/options.c:766
msgid "Number of Pictures to capture"
msgstr "Кількість знімків"

#: ../src/options.c:767
msgid "Video File name (capture from start)"
msgstr "Файл відео (запис зразу після запуску)"

#: ../src/options.c:768
msgid "Video capture time (in seconds)"
msgstr "Тривалість запису (сек.)"

#: ../src/options.c:769
msgid "Exits guvcview after closing video"
msgstr "Вийти з Guvcview після закриття відео"

#: ../src/options.c:770
msgid "Number of initial frames to skip"
msgstr "Кількість початкових кадрів для пропуску"

#: ../src/options.c:771
msgid "Show FPS value (enable(1) | disable (0))"
msgstr "Відображення частоти кадрів (увімкн.(1) | вимкн. (0))"

#: ../src/options.c:772
msgid "Load Profile at start"
msgstr "Завантажити профіль при запуску"

#: ../src/options.c:773
msgid "List controls method (0:loop, 1:next_ctrl flag [def])"
msgstr ""

#: ../src/options.c:779
msgid "- local options"
msgstr "- локальні параметри"

#. gdk_threads_enter();
#: ../src/timers.c:119
msgid "Cap. Image"
msgstr "Знімок"

#: ../src/timers.c:217
msgid "Guvcview Warning:"
msgstr "Попередження:"

#: ../src/timers.c:217
msgid "Not enough free space left on disk"
msgstr "Не вистачає вільного місця на диску"

#: ../src/v4l2_controls.c:794
msgid "Left"
msgstr "Лівий"

#: ../src/v4l2_controls.c:795
msgid "Right"
msgstr "Правий"

#: ../src/v4l2_controls.c:799
msgid "Down"
msgstr "Вниз"

#: ../src/v4l2_controls.c:800
msgid "Up"
msgstr "Вверх"

#: ../src/v4l2_controls.c:845
msgid "Off"
msgstr ""

#: ../src/v4l2_controls.c:845
msgid "On"
msgstr ""

#: ../src/v4l2_controls.c:845
msgid "Blinking"
msgstr ""

#: ../src/v4l2_controls.c:845
msgid "Auto"
msgstr ""

#. turn it into a menu control
#: ../src/v4l2_controls.c:894
msgid "8 bit"
msgstr ""

#: ../src/v4l2_controls.c:894
msgid "12 bit"
msgstr ""

#: ../src/v4l2_dyna_ctrls.c:96
msgid "Pan (relative)"
msgstr "Відносний поворот"

#: ../src/v4l2_dyna_ctrls.c:109
msgid "Tilt (relative)"
msgstr "Відносний нахил"

#: ../src/v4l2_dyna_ctrls.c:122
msgid "Pan Reset"
msgstr "Скинути поворот"

#: ../src/v4l2_dyna_ctrls.c:135
msgid "Tilt Reset"
msgstr "Скинути нахил"

#: ../src/v4l2_dyna_ctrls.c:148
msgid "Focus (absolute)"
msgstr "Абсолютний фокус"

#: ../src/v4l2_dyna_ctrls.c:161
msgid "LED1 Mode"
msgstr "Метод світлодіода"

#: ../src/v4l2_dyna_ctrls.c:174
msgid "LED1 Frequency"
msgstr "Частота світлодіода"

#: ../src/v4l2_dyna_ctrls.c:187
msgid "Disable video processing"
msgstr "Відключити обробку відео"

#: ../src/v4l2_dyna_ctrls.c:200
msgid "Raw bits per pixel"
msgstr "Сирі біти на піксель"

#. needed only for language files (not used)
#. V4L2 control strings
#: ../src/v4l2uvc.c:57
msgid "User Controls"
msgstr "Керуючі елементи користувача"

#: ../src/v4l2uvc.c:58
msgid "Brightness"
msgstr "Яскравість"

#: ../src/v4l2uvc.c:59
msgid "Contrast"
msgstr "Контраст"

#: ../src/v4l2uvc.c:60
msgid "Hue"
msgstr "Відтінок"

#: ../src/v4l2uvc.c:61
msgid "Saturation"
msgstr "Насиченість"

#: ../src/v4l2uvc.c:62
msgid "Sharpness"
msgstr "Різкість"

#: ../src/v4l2uvc.c:63
msgid "Gamma"
msgstr "Гамма"

#: ../src/v4l2uvc.c:64
msgid "Backlight Compensation"
msgstr "Компенсація підсвітки"

#: ../src/v4l2uvc.c:65
msgid "Power Line Frequency"
msgstr "Частота електромережі"

#: ../src/v4l2uvc.c:66
msgid "Hue, Automatic"
msgstr "Авто відтінок"

#: ../src/v4l2uvc.c:67
msgid "Focus, Auto"
msgstr "Авто фокус"

#: ../src/v4l2uvc.c:68
msgid "Manual Mode"
msgstr "Ручний режим"

#: ../src/v4l2uvc.c:69
msgid "Auto Mode"
msgstr "Авторежим"

#: ../src/v4l2uvc.c:70
msgid "Shutter Priority Mode"
msgstr "Пріоритет затвору"

#: ../src/v4l2uvc.c:71
msgid "Aperture Priority Mode"
msgstr "Пріоритет діафрагми"

#: ../src/v4l2uvc.c:72
msgid "Black Level"
msgstr "Рівень чорного"

#: ../src/v4l2uvc.c:73
msgid "White Balance, Automatic"
msgstr "Авто баланс білого"

#: ../src/v4l2uvc.c:74
msgid "Do White Balance"
msgstr "Збалансувати білий"

#: ../src/v4l2uvc.c:75
msgid "Red Balance"
msgstr "Баланс червоного"

#: ../src/v4l2uvc.c:76
msgid "Blue Balance"
msgstr "Баланс синього"

#: ../src/v4l2uvc.c:77
msgid "Exposure"
msgstr "Експозиція"

#: ../src/v4l2uvc.c:78
msgid "Gain, Automatic"
msgstr "Авто посилення"

#: ../src/v4l2uvc.c:79
msgid "Gain"
msgstr "Посилення"

#: ../src/v4l2uvc.c:80
msgid "Horizontal Flip"
msgstr "Горизонтальне віддзеркалення"

#: ../src/v4l2uvc.c:81
msgid "Vertical Flip"
msgstr "Вертикальне віддзеркалення"

#: ../src/v4l2uvc.c:82
msgid "Horizontal Center"
msgstr "Горизонтальна середина"

#: ../src/v4l2uvc.c:83
msgid "Vertical Center"
msgstr "Вертикальна середина"

#: ../src/v4l2uvc.c:84
msgid "Chroma AGC"
msgstr "Chroma AGC"

#: ../src/v4l2uvc.c:85
msgid "Color Killer"
msgstr "Подавлювач кольору"

#: ../src/v4l2uvc.c:86
msgid "Color Effects"
msgstr "Ефекти кольору"

#. CAMERA CLASS control strings
#: ../src/v4l2uvc.c:89
msgid "Camera Controls"
msgstr "Керування камерою"

#: ../src/v4l2uvc.c:90
msgid "Auto Exposure"
msgstr "Авто експозиція"

#: ../src/v4l2uvc.c:91
msgid "Exposure Time, Absolute"
msgstr "Абсолютний час експозиції"

#: ../src/v4l2uvc.c:92
msgid "Exposure, Dynamic Framerate"
msgstr "Експозиція, динамічна частота кадрів"

#: ../src/v4l2uvc.c:93
msgid "Pan, Relative"
msgstr "Відносний поворот"

#: ../src/v4l2uvc.c:94
msgid "Tilt, Relative"
msgstr "Відносний нахил"

#: ../src/v4l2uvc.c:95
msgid "Pan, Reset"
msgstr "Скинути поворот"

#: ../src/v4l2uvc.c:96
msgid "Tilt, Reset"
msgstr "Скинути нахил"

#: ../src/v4l2uvc.c:97
msgid "Pan, Absolute"
msgstr "Абсолютний поворот"

#: ../src/v4l2uvc.c:99
msgid "Focus, Absolute"
msgstr "Абсолютний фокус"

#: ../src/v4l2uvc.c:100
msgid "Focus, Relative"
msgstr "Відносний фокус"

#: ../src/v4l2uvc.c:101
msgid "Focus, Automatic"
msgstr "Авто фокус"

#: ../src/v4l2uvc.c:102
msgid "Zoom, Absolute"
msgstr "Абсолютний зум"

#: ../src/v4l2uvc.c:103
msgid "Zoom, Relative"
msgstr "Відносний зум"

#: ../src/v4l2uvc.c:104
msgid "Zoom, Continuous"
msgstr "Безперервний зум"

#: ../src/v4l2uvc.c:105
msgid "Privacy"
msgstr "Конфіденційність"

#. UVC specific control strings
#: ../src/v4l2uvc.c:108
msgid "Exposure, Auto"
msgstr "Авто експозиція"

#: ../src/v4l2uvc.c:109
msgid "Exposure, Auto Priority"
msgstr "Пріоритет авто експозиції"

#: ../src/v4l2uvc.c:110
msgid "Exposure (Absolute)"
msgstr "Абсолютна експозиція"

#: ../src/v4l2uvc.c:111
msgid "White Balance Temperature, Auto"
msgstr "Авто температура балансу білого"

#: ../src/v4l2uvc.c:112
msgid "White Balance Temperature"
msgstr "Температура балансу білого"

#: ../src/v4l2uvc.c:113
msgid "White Balance Component, Auto"
msgstr "Авто компонент балансу білого"

#: ../src/v4l2uvc.c:114
msgid "White Balance Blue Component"
msgstr "Синій компонент балансу білого"

#: ../src/v4l2uvc.c:115
msgid "White Balance Red Component"
msgstr "Червоний компонент балансу білого"

#. libwebcam specific control strings
#: ../src/v4l2uvc.c:118
msgid "Focus"
msgstr "Фокус"

#: ../src/v4l2uvc.c:119
msgid "Focus (Absolute)"
msgstr "Абсолютний фокус"

#: ../src/vcodecs.c:63
msgid "MJPG - compressed"
msgstr "MJPG - стиснено"

#: ../src/vcodecs.c:99
msgid "YUY2 - uncomp YUV"
msgstr "YUY2 - не стиснений YUV"

#: ../src/vcodecs.c:135
msgid "RGB - uncomp BMP"
msgstr "RGB - не стиснений BMP"

#: ../src/vcodecs.c:171
msgid "MPEG video 1"
msgstr "MPEG-відео 1"

#: ../src/vcodecs.c:207
msgid "FLV1 - flash video 1"
msgstr "FLV1 флеш-відео 1"

#: ../src/vcodecs.c:243
msgid "WMV1 - win. med. video 7"
msgstr "WMV1 - відео Windows Media 7"

#: ../src/vcodecs.c:279
msgid "MPG2 - MPG2 format"
msgstr "MPG2 - формат MPG2"

#: ../src/vcodecs.c:315
msgid "MS MP4 V3"
msgstr "MS MP4 V3"

#: ../src/vcodecs.c:351
msgid "MPEG4-ASP"
msgstr "MPEG4-ASP"

#: ../src/vcodecs.c:387
msgid "MPEG4-AVC (H264)"
msgstr "MPEG4-AVC (H264)"

#: ../src/vcodecs.c:427
msgid "VP8 (VP8)"
msgstr ""

#: ../src/vcodecs.c:463
msgid "Theora (ogg theora)"
msgstr ""

#: ../src/video_format.c:43
msgid "AVI - avi format"
msgstr ""

#: ../src/video_format.c:52
msgid "MKV - Matroska format"
msgstr ""

#: ../src/video_format.c:61
msgid "WEBM - format"
msgstr ""

#: ../src/video_tab.c:216
msgid "Frame Rate:"
msgstr "Частота кадрів:"

#: ../src/video_tab.c:223
msgid " Show"
msgstr " Показати"

#: ../src/video_tab.c:245
msgid "Resolution:"
msgstr "Роздільна здатність:"

#: ../src/video_tab.c:271
msgid "Camera Output:"
msgstr "Вивід камери:"

#: ../src/video_tab.c:293
msgid "Apply"
msgstr "Застосувати"

#: ../src/video_tab.c:300
msgid "Quality:"
msgstr "Якість:"

#: ../src/video_tab.c:310
msgid "---- Video Filters ----"
msgstr "---- Відео фільтри ----"

#. Mirror
#: ../src/video_tab.c:329
msgid " Mirror"
msgstr " Гор. віддзеркал."

#. Upturn
#: ../src/video_tab.c:340
msgid " Invert"
msgstr " Верт. віддзеркал."

#. Negate
#: ../src/video_tab.c:351
msgid " Negative"
msgstr " Негатив"

#. Mono
#: ../src/video_tab.c:362
msgid " Mono"
msgstr " Чорно-біле"

#. Pieces
#: ../src/video_tab.c:374
msgid " Pieces"
msgstr " Мозаїка"

#. Particles
#: ../src/video_tab.c:386
msgid " Particles"
msgstr " Часточки"

#: ../src/uvc_h264.c:596
msgid "H264 Controls"
msgstr ""

#: ../src/uvc_h264.c:615
msgid "Rate Control Mode:"
msgstr ""

#: ../src/uvc_h264.c:626
msgid "CBR"
msgstr ""

#: ../src/uvc_h264.c:629
msgid "VBR"
msgstr ""

#: ../src/uvc_h264.c:633
msgid "Constant QP"
msgstr ""

#: ../src/uvc_h264.c:651
msgid "Rate Control Mode flags:"
msgstr ""

#: ../src/uvc_h264.c:676
msgid "Temporal Scale Mode:"
msgstr ""

#: ../src/uvc_h264.c:704
msgid "Spatial Scale Mode:"
msgstr ""

#: ../src/uvc_h264.c:737
msgid "Frame Interval (100ns units):"
msgstr ""

#: ../src/uvc_h264.c:772
msgid "Bit Rate:"
msgstr ""

#: ../src/uvc_h264.c:795
msgid "Hints:"
msgstr ""

#: ../src/uvc_h264.c:800
msgid "Resolution"
msgstr ""

#: ../src/uvc_h264.c:804
msgid "Profile"
msgstr ""

#: ../src/uvc_h264.c:808
msgid "Rate Control"
msgstr ""

#: ../src/uvc_h264.c:812
msgid "Usage Type"
msgstr ""

#: ../src/uvc_h264.c:816
msgid "Slice Mode"
msgstr ""

#: ../src/uvc_h264.c:820
msgid "Slice Unit"
msgstr ""

#: ../src/uvc_h264.c:824
msgid "MVC View"
msgstr ""

#: ../src/uvc_h264.c:828
msgid "Temporal Scale"
msgstr ""

#: ../src/uvc_h264.c:832
msgid "SNR Scale"
msgstr ""

#: ../src/uvc_h264.c:836
msgid "Spatial Scale"
msgstr ""

#: ../src/uvc_h264.c:840
msgid "Spatial Layer Ratio"
msgstr ""

#: ../src/uvc_h264.c:844
msgid "Frame Interval"
msgstr ""

#: ../src/uvc_h264.c:848
msgid "Leaky Bucket Size"
msgstr ""

#: ../src/uvc_h264.c:852
msgid "Bit Rate"
msgstr ""

#: ../src/uvc_h264.c:856 ../src/uvc_h264.c:1250
msgid "CABAC"
msgstr ""

#: ../src/uvc_h264.c:860
msgid "(I) Frame Period"
msgstr ""

#: ../src/uvc_h264.c:907
msgid "Slice Mode:"
msgstr ""

#: ../src/uvc_h264.c:914
msgid "no multiple slices"
msgstr ""

#: ../src/uvc_h264.c:916
msgid "bits/slice"
msgstr ""

#: ../src/uvc_h264.c:918
msgid "Mbits/slice"
msgstr ""

#: ../src/uvc_h264.c:920
msgid "slices/frame"
msgstr ""

#: ../src/uvc_h264.c:929
msgid "Slice Units:"
msgstr ""

#: ../src/uvc_h264.c:950
msgid "Profile:"
msgstr ""

#: ../src/uvc_h264.c:957
msgid "Baseline Profile"
msgstr ""

#: ../src/uvc_h264.c:959
msgid "Main Profile"
msgstr ""

#: ../src/uvc_h264.c:961
msgid "High Profile"
msgstr ""

#: ../src/uvc_h264.c:963
msgid "Scalable Baseline Profile"
msgstr ""

#: ../src/uvc_h264.c:965
msgid "Scalable High Profile"
msgstr ""

#: ../src/uvc_h264.c:967
msgid "Multiview High Profile"
msgstr ""

#: ../src/uvc_h264.c:969
msgid "Stereo High Profile"
msgstr ""

#: ../src/uvc_h264.c:1008
msgid "Profile flags:"
msgstr ""

#: ../src/uvc_h264.c:1033
msgid "(I) Frame Period (ms):"
msgstr ""

#: ../src/uvc_h264.c:1054
msgid "Estimated Video Delay (ms):"
msgstr ""

#: ../src/uvc_h264.c:1075
msgid "Estimated Max Config Delay (ms):"
msgstr ""

#: ../src/uvc_h264.c:1096
msgid "Usage Type:"
msgstr ""

#: ../src/uvc_h264.c:1103
msgid "Real-time"
msgstr ""

#: ../src/uvc_h264.c:1105
msgid "Broadcast"
msgstr ""

#: ../src/uvc_h264.c:1107
msgid "Storage"
msgstr ""

#: ../src/uvc_h264.c:1109
msgid "(0) Non-scalable single layer AVC"
msgstr ""

#: ../src/uvc_h264.c:1111
msgid "(1) SVC temporal scalability with hierarchical P"
msgstr ""

#: ../src/uvc_h264.c:1113
msgid "(2q) SVC temporal scalability + Quality/SNR scalability"
msgstr ""

#: ../src/uvc_h264.c:1115
msgid "(2s) SVC temporal scalability + spatial scalability"
msgstr ""

#: ../src/uvc_h264.c:1117
msgid "(3) Full SVC scalability"
msgstr ""

#: ../src/uvc_h264.c:1131
msgid "SNR Control Mode:"
msgstr ""

#: ../src/uvc_h264.c:1138
msgid "No SNR Enhancement Layer"
msgstr ""

#: ../src/uvc_h264.c:1140
msgid "CGS NonRewrite (2 Layer)"
msgstr ""

#: ../src/uvc_h264.c:1142
msgid "CGS NonRewrite (3 Layer)"
msgstr ""

#: ../src/uvc_h264.c:1144
msgid "CGS Rewrite (2 Layer)"
msgstr ""

#: ../src/uvc_h264.c:1146
msgid "CGS Rewrite (3 Layer)"
msgstr ""

#: ../src/uvc_h264.c:1148
msgid "MGS (2 Layer)"
msgstr ""

#: ../src/uvc_h264.c:1173
msgid "Stream Mux Enable"
msgstr ""

#: ../src/uvc_h264.c:1180
msgid "Embed H.264 aux stream"
msgstr ""

#: ../src/uvc_h264.c:1182
msgid "Embed YUY2 aux stream"
msgstr ""

#: ../src/uvc_h264.c:1184
msgid "Embed NV12 aux stream"
msgstr ""

#: ../src/uvc_h264.c:1204
msgid "MJPG payload container"
msgstr ""

#: ../src/uvc_h264.c:1219
msgid "Stream Format:"
msgstr ""

#: ../src/uvc_h264.c:1227
msgid "Byte stream format (H.264 Annex-B)"
msgstr ""

#: ../src/uvc_h264.c:1229
msgid "NAL stream format"
msgstr ""

#: ../src/uvc_h264.c:1240
msgid "Entropy CABAC:"
msgstr ""

#: ../src/uvc_h264.c:1248
msgid "CAVLC"
msgstr ""

#: ../src/uvc_h264.c:1261
msgid "Picture timing SEI"
msgstr ""

#: ../src/uvc_h264.c:1268
msgid "B Frames:"
msgstr ""

#: ../src/uvc_h264.c:1293
msgid "Preview Flipped"
msgstr ""

#: ../src/uvc_h264.c:1300
msgid "Additional MVC Views:"
msgstr ""

#: ../src/uvc_h264.c:1325
msgid "Simulcast stream index:"
msgstr ""

#: ../src/uvc_h264.c:1350
msgid "Spatial Layer Ratio:"
msgstr ""

#: ../src/uvc_h264.c:1379
msgid "Leaky Bucket Size (ms):"
msgstr ""

#. encoder reset
#: ../src/uvc_h264.c:1406
msgid "Encoder Reset"
msgstr ""

#. 
#. h264_controls->probe_button = gtk_button_new_with_label(_("PROBE"));
#. g_signal_connect (GTK_BUTTON(h264_controls->probe_button), "clicked",
#. G_CALLBACK (h264_probe_button_clicked), all_data);
#. 
#. gtk_grid_attach (GTK_GRID(table), h264_controls->probe_button, 0, line, 1 ,1);
#. gtk_widget_show(h264_controls->probe_button);
#. 
#: ../src/uvc_h264.c:1421
msgid "COMMIT"
msgstr ""
